# Checkout Converter

Provide an endpoint that will consume the Fixer.io API to calculate the cart value at current exchange rates, rounded to 2 decimal places.


## API Documentation

https://wispopsconverter.docs.apiary.io/#


