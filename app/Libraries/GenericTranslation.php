<?php

namespace App\Libraries;

use App\Models\User;

trait GenericTranslation {
    public function translations() {
		return $this->morphMany('App\Models\Translation', 'translatable');
	}

    public function getLocalizedSlugAttribute() {
		 return $this->translations()->whereLanguage('fr')->first()->title;
	}

	// public function scopeWithUniqueSlugConstraints($query, $model, $attribute, $config, $slug)
    // {
    //     return $query->where('type', $model->type)->whereCompleted(1);
    // }

    public function getLocalizedTitleAttribute() {
        return $this->getLocalized('title');
    }

    public function getLocalizedSubtitleAttribute() {
        return $this->getLocalized('subtitle');
    }

    public function getLocalizedIntroductionAttribute() {
        return $this->getLocalized('introduction');
    }

    public function getLocalizedContentAttribute() {
        return $this->getLocalized('content');
    }

    public function getLocalizedConclusionAttribute() {
		return $this->getLocalized('conclusion');
	}

    private function getLocalized($attribute) {
        $language = \App::getLocale();
		$translate = '';

		if($this->translations()->count()) {
			if($this->translations()->whereLanguage($language)->count()) {
				$translate = $this->translations()->whereLanguage($language)->first()->{$attribute};
			}
		}

		if(!$translate) {
			$translate = $this->translations()->whereLanguage('fr')->first()->{$attribute};
		}

		return $translate;
    }

}

 ?>
