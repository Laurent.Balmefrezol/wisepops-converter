<?php


namespace App\Libraries;


class Fixer
{

    /*
     * checkout currency
     */
    protected  $checkout_currency;
    /*
     * List of Items
     */
    protected  $items;

    public function __construct(string $checkout_currency, array $items){

        $this->checkout_currency = $checkout_currency;
        $this->items = collect($items);
    }

    /**
     *
     * Get latest rates for the checkout currency
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function _getLastRates(): array {

        $client = new \GuzzleHttp\Client();

        //list of currencies we need for rates change calculation (expect checkout_currency)
        $checkout_currency = $this->checkout_currency;
        $currencies_rates_asked = $this->items->filter(function ($value, $key) use ($checkout_currency) {
            return $value['currency'] != $checkout_currency;
        })->pluck('currency');

        //send request to fixer.io
        $res = $client->request(
            'GET',
            config('fixer-io.full_url').'&base='.$this->checkout_currency.'&symbols='.$currencies_rates_asked->implode(',')
        );

        return (array) json_decode($res->getBody())->rates;
    }

    /**
     * Sum of cart with currencies rates
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkoutPrice() : float {

        //list of the currencies rates
        $rates = $this->_getLastRates();
        $sum = 0;

        foreach ($this->items as $item){

            if($item['currency'] == $this->checkout_currency){
                $sum += $item['price']*$item['quantity'];
            }else{
                // use currency rate
                $sum += $item['price']*1/$rates[$item['currency']]*$item['quantity'];
            }
        }

        return round($sum,2);
    }

}