<?php

namespace App\Libraries;

use App\Models\User;

trait GenericPolicy {

    public function before(User $user, $ability){
        if ($user->abilities->where('id', $this->module_id)->first() == null) {
            return true;
        }
    }

    public function read(User $user){
        return $user->abilities->where('id', $this->module_id)->first()->pivot->ability >= 1;
    }

    public function store(User $user){
        return $user->abilities->where('id', $this->module_id)->first()->pivot->ability >= 2;
    }

    public function delete(User $user){
        return $user->abilities->where('id', $this->module_id)->first()->pivot->ability >= 3;
    }

}

 ?>
