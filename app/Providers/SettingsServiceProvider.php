<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Models\Setting;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Factory $cache, Setting $settings)
    {
        // $settings = $cache->remember('settings', 60, function() use ($settings) {
        //     return $settings->pluck('value', 'id')->all();
        // });

        // TODO: décommenter les lignes ci-dessous pour utiliser le cache (et supprimer la ligne ci-dessous)
        $settings = $settings->pluck('value', 'id')->all();

        config()->set('settings', $settings);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
