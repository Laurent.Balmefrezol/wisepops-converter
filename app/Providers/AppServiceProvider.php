<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Ignore Error HASTAFIX
        error_reporting(0);

        Validator::extend('correct_password', 'App\Http\CustomValidator@correctPassword');
        Validator::extend('valid_login', 'App\Http\CustomValidator@validLogin');
        Validator::extend('valid_repository', 'App\Http\CustomValidator@validRepository');
        Validator::extend('recaptcha','App\Validators\ReCaptcha@validate');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
