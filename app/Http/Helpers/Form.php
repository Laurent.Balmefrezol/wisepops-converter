<?php
/*
 * Version 2.00 Laravel
 */

class Form{

	public static $view_folder = 'libraries.formulaires.';

	private static function get_champ($type = 'input', $name = 'input', $label = 'input', $params = array()) {

		$data['champ']->type = $type; // input, password, select, textarea, file, ckeditor
		$data['champ']->nom = $name;
		$data['champ']->libelle = $label;

		// PARAMETRES
		$data['champ']->layout = 'row'; // row = label au dessus du champ; line = label et champ sur la même ligne
		$data['champ']->options = array(); // options du champ select sous la forme array('value' => 'Libellé')
		$data['champ']->options_selected = array(); // options du champ select qui seront sélectionnées sous la forme array('' => 'ID')
		$data['champ']->required = false; // champ obligatoire ou non
		$data['champ']->disabled = false; // champ désactivé ou non
		$data['champ']->readonly = false; // champ désactivé ou non
		$data['champ']->show_error = true; // afficher le message d'erreur form_validation ou non
		$data['champ']->show_label = true; // afficher le label (intitulé du champ) ou non
		$data['champ']->show_input = true; // afficher le champ (true) ou seulement la valeur du champ en texte brut (false)
		$data['champ']->hidden = false; // champ caché ou non
		$data['champ']->default = false; // valeur par défaut du champ
		$data['champ']->force_default = false; // forcer la valeur par défaut
		$data['champ']->datepicker = false; // champ de type jquery datepicker ou non
		$data['champ']->datetimepicker = false; // champ de type jquery datetimepicker ou non
		$data['champ']->date_format = 'd/m/Y'; // format de la date du datepicker
		$data['champ']->datetime_format = 'd/m/Y H:i'; // format de la date du datetimepicker
		$data['champ']->link = false; // lien vers la dépendance ou non
		$data['champ']->width = false; // largeur du champ textarea, exemple :"300px" ou "100%"
		$data['champ']->height = false; // hauteur du champ textarea
		$data['champ']->maxlength = false; // hauteur du champ textarea
		//$data['champ']->element = 'element'; // nom de la variable pour l'élément en cours de modification
		$data['champ']->multiple = false; // permet de créer un champ select à choix multiple
		$data['champ']->class = ''; // liste des classes supplémentaires à ajouter au champ (séparées par des espaces)
		$data['champ']->id = $name; // id du champ, à renseigner seulement s'il est différent du name
		$data['champ']->tooltip_content = false; // afficher le label (intitulé du champ) ou non
		$data['champ']->details = false; // rajoute une ligne explicative en dessous du label
		$data['champ']->data_html = array(); // ajoute des data-* (HTML5) exemple array('geo' => 'adresse', 'fruit' => 'kiwi, 'dj' => 'Pitouf');
		$data['champ']->placeholder = false;
		$data['champ']->autosave = false; // Activer l'autosave de Froala
		$data['champ']->autosave_url = false;
		$data['champ']->autosave_language = false;
		foreach($params as $key=>$row) {
			$data['champ']->{$key} = $row;
		}

		switch($type) {
			case 'input' :
				return view(self::$view_folder.'input',$data);
			break;

			case 'password' :
				return view(self::$view_folder.'input',$data);
			break;

			case 'select' :
				return view(self::$view_folder.'select',$data);
			break;

			case 'textarea' :
				return view(self::$view_folder.'textarea',$data);
			break;

			case 'ckeditor' :
				return view(self::$view_folder.'ckeditor',$data);
			break;

			case 'file' :
				return view(self::$view_folder.'file',$data);
			break;

		}
	}

	private static function get_submit($libelle = 'submit', $params = array()) {
		$data['champ']->libelle = $libelle;
		// PARAMETRES
		$data['champ']->class = '';
		foreach($params as $key=>$row) {
			$data['champ']->{$key} = $row;
		}
			return view(self::$view_folder.'submit',$data);
	}


	static function image_input($module, $params = []) {

		$defaults = [
		'module' => $module,
		'element' => null,
		'function' => 'ajax_upload_img',
		'show_update' => true,
		'show_delete' => true,
		];

		return view(self::$view_folder.'image_input',array_merge($defaults, $params));
	}


	static function input($name = 'input', $label = 'input', $params = array()) {
		return self::get_champ('input', $name, $label, $params);
	}

	static function password($name = 'password', $label = 'password', $params = array()) {
		return self::get_champ('password', $name, $label, $params);
	}

	static function select($name = 'select', $label = 'select', $params = array()) {
		return self::get_champ('select', $name, $label, $params);
	}

	static function textarea($name = 'textarea', $label = 'textarea', $params = array()) {
		return self::get_champ('textarea', $name, $label, $params);
	}

	static function ckeditor($name = 'ckeditor', $label = 'ckeditor', $params = array()) {
		return self::get_champ('ckeditor', $name, $label, $params);
	}

	static function file($name = 'file', $label = 'file', $params = array()) {
		return self::get_champ('file', $name, $label, $params);
	}

	static function recaptcha() {
		return view(self::$view_folder.'recaptcha');
	}

	static function submit($libelle = 'submit', $params = array()) {
		return self::get_submit($libelle, $params);
	}

	static function listOptgroup($list){
		$return = [];
		foreach ($list as $item){
			$return[$item->optgroup][$item->id] = $item->value;
		}
		return $return;
	}

}
