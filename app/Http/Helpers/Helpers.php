<?php // Code within app\Helpers\Helper.php

use Carbon\Carbon;

function clean($string) {
    return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $string);
}

function img_url($img, $theme = false){
    if(!$theme) {
        return asset('images/'.$img);
    }else {
        return asset('themes/'.config('settings.theme').'/images/'.$img);
    }

}
/**
 * This function will add http&#58;// in the event that a protocol prefix is missing from a URL.
 * @param  char $url [description]
 * @return char
 */
function prep_url($url){
  if (substr($url, 0, 4 ) !== "http"){
    return 'http://'.$url;
  }else{
    return $url;
  }
}

/*
 * Retourne la l'attribut $attribute de l'objet $object s'il existe, vide sinon
 */
function h_value($object,$attribute,$action = FALSE)
{
  if(is_object($object)) {
        if(isset($object->{$attribute})){ //dans l'eloquent la propriété n'existe pas
        //if(property_exists($object,$attribute)) {
            switch($action) {
                case 'strip_tags': $object->{$attribute} = strip_tags($object->{$attribute});

                default: break;
            }
            return $object->{$attribute};
        }else {
            return '';
        }
    }else {
        return '';
    }
}

/*
    Transforme une date fr au format YY-mm-dd H:i:s
*/
function hIntDate($d) {
	if($d!='') {
        if(strpos($d, '/') !== false) {
    		$date = explode(' ',$d);
    		$jour = explode('/',$date[0]);
    		$heure = explode(':',$date[1]);
            $heure[0] = $heure[0]?$heure[0]:'00';
            $heure[1] = $heure[1]?$heure[1]:'00';
    		return $jour[2].'-'.$jour[1].'-'.$jour[0].' '.$heure[0].':'.$heure[1].':00';
        }else {
            $date = explode(' ',$d);
    		$jour = explode('-',$date[0]);
    		$heure = explode(':',$date[1]);
            $heure[0] = $heure[0]?$heure[0]:'00';
            $heure[1] = $heure[1]?$heure[1]:'00';
    		return $jour[0].'-'.$jour[1].'-'.$jour[2].' '.$heure[0].':'.$heure[1].':00';
        }
	}else {
		return '';
	}
}

function h_display_ckeditor($data)
{
	$return = '';
	//if(!isset($data['multiple'])) {
        $return = '<script src="'.url('vendor/unisharp/laravel-ckeditor/ckeditor.js').'"></script>';
    	//$return = '<script type="text/javascript" src="'.url('/').'/'.$data['path'] . '/ckeditor.js"></script>';
	//}

    $return .= '<script>
                    CKEDITOR.replace("'.$data['id'].'", {';

                        //On ajoute le fichier de config custom
                        if(isset($data['customConfig'])){
                            $return .= 'customConfig: "vendor/unisharp/laravel-ckeditor/'.$data['customConfig'].'",';
                        }else{
                            $return .= 'customConfig: "config.js",';
                        }

                        $return .= 'filebrowserImageUploadUrl: "'.url('laravel-filemanager/upload?type=Images&_token='.csrf_token()).'",
                        filebrowserUploadUrl: "'.url('laravel-filemanager/upload?type=Files&_token='.csrf_token()).'",';

                        //Adding config values
                        if(isset($data['config'])) {
                            foreach($data['config'] as $k=>$v) {

                                if($k != null){
                                    $return .= $k . " : '" . $v . "'";
                                }

                                if($k !== end(array_keys($data['config']))) {
                                    $return .= ",";
                                }
                            }
                        }

    $return .= '});';
    $return .= 'CKEDITOR.config.stylesCombo_stylesSet = "my_styles"';
    $return .= '</script>';

    //Adding styles values
    if(isset($data['styles'])) {
    	$return .= '<script type="text/javascript">CKEDITOR.addStylesSet( "my_styles", [';

	    foreach($data['styles'] as $k=>$v) {

	    	$return .= '{ name : "' . $k . '", element : "' . $v['element'] . '", styles : { ';

	    	if(isset($v['styles'])) {
	    		foreach($v['styles'] as $k2=>$v2) {

	    			$return .= '"' . $k2 . ' " : " ' . $v2 . '"';

					if($k2 !== end(array_keys($v['styles']))) {
						 $return .= ',';
					}
	    		}
    		}

	    	$return .= '} }';

  	  	if($k !== end(array_keys($data['styles']))) {
  				$return .= ',';
  			}
	    }

	    $return .= ']);</script>';
    }

    return $return;
}

function hDateTimeFormat($date, $display = 'lg', $human = true) {
    $lang = LaravelLocalization::getCurrentLocale();
    Carbon::setLocale($lang);

    if($human) {
        return $date->diffForHumans();
    }else {
        switch ($lang) {
            case 'en':
                return $display == 'lg' ? $date->format('d M y, H:i') : $date->format('d M y');
            break;

            case 'fr':
                return $display == 'lg' ? $date->format('d/m/y à H:i') : $date->format('d/m/Y');
            break;

            default:
                return $display == 'lg' ? $date->format('d M y, H:i') : $date->format('d M y');
            break;
        }
    }
}
