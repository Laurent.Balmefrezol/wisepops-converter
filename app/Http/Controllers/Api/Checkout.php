<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\Fixer;
use \App\Http\Requests\Cart as CartRequest;
use Response;


class Checkout extends Controller {

    public function price(CartRequest $request){

        //use Fixer.io lib
        //@TODO create factory if you need multiple providers
        $fixer = new Fixer($request['checkoutCurrency'],$request['items']);

        return Response::json([
            'checkoutPrice' => $fixer->checkoutPrice(),
            'checkoutCurrency' => $request['checkoutCurrency']
        ]);

    }


}
