<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;

use App\Models\User;
class AuthController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request) {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true)) {
            $json['url'] = route('dashboard::index');
        }

        return $json;
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
