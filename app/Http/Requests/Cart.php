<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class Cart extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
           'items' => 'required',
           'checkoutCurrency' => 'required',
           'items.*.price' => 'required|numeric',
           'items.*.quantity' => 'required|integer',
           'items.*.currency' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }
}
