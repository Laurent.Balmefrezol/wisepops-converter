<?php
namespace App\Http;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomValidator {

    public function correctPassword($attribute, $value, $parameters, $validator) {
        return Hash::check($value, Auth::user()->password);
    }

    public function validLogin($attribute, $value, $parameters, $validator) {
        return preg_match('/^[A-Za-z][A-Za-z0-9_-]{2,14}$/', $value);
    }

    public function validRepository($attribute, $value, $parameters, $validator) {
        return strpos($value, 'github') !== false
         || strpos($value, 'bitbucket') !== false
         || strpos($value, 'cloudforge') !== false
         || strpos($value, 'launchpad') !== false
         || strpos($value, 'assembla') !== false
         || strpos($value, 'codeplex') !== false
         || strpos($value, 'ccpforge') !== false
         || strpos($value, 'freepository') !== false
         || strpos($value, 'google') !== false
         || strpos($value, 'codebase') !== false
         || strpos($value, 'jenkins') !== false
         || strpos($value, 'repository') !== false
         || strpos($value, 'unfuddle') !== false
         || strpos($value, 'projectlocker') !== false;
    }
}
