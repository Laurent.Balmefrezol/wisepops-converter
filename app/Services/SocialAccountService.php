<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\SocialAccount;
use App\Models\User;
use App\Models\Image;
use Storage, Mail, Session;
use App\Mail\SignUp;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider = null)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'social_login' => $providerUser->getNickname(),
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                    'user_group_id' => 2,
                    'active' => 1
                ]);

                if($user->avatar) {
                    $filename = sha1($user->id.$user->avatar).'.jpg';
                    Storage::disk('public')->put('upload/images/users/'.$filename, file_get_contents($user->avatar));

                    $image = new Image;
                    $image->name = $filename;
                    $image->module = 'users';
                    $image->save();

                    $user->image_id = $image->id;
                    $user->save();
                }
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}
