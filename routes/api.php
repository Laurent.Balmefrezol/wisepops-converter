<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//@TODO we can use ip restriction middleware to protect this endpoint
// exemple : 'ip.restriction:127.0.0.1,192.168.*'
Route::group(['middleware' => []], function(){

    Route::get('/ping', function(){
        return Response::json('pong');
    });

    Route::group(['prefix' => 'checkout', 'as'=> 'checkout::'], function() {
        Route::post('/price', 'Api\Checkout@price')->name('price');
    });

});
