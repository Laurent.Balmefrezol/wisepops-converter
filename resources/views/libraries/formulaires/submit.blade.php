<?php
$affiche_data_html = null;

if(count($champ->data_html)){
	foreach ($champ->data_html as $key => $data_html)	{
		$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
	}
}

?>
<div class="element form-group submit noprint">
	<input type="submit" value="<?php echo $champ->libelle?>" class="btn btn-primary btn-md <?php echo $champ->class?>" />
</div>
