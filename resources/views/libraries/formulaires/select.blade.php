<div class="row form-group element <?php if($errors->has($champ->nom) != ''):?>form-error<?php endif;?> layout-<?php echo $champ->layout?>" id="formelement-<?php echo $champ->id?>">
	<?php if($champ->layout == 'line'):?>
		<div class="col-sm-3">
	<?php else:?>
		<div class="col-sm-12">
	<?php endif;?>
		<?php if($champ->show_label):?>
			<label for="<?php echo $champ->nom?>">
				<?php echo $champ->libelle?>

				<?php if($champ->tooltip):?>
					&nbsp;
					<i class="fa fa-question-circle tool-tip fs-18 cpointer" title="<?php echo $champ->tooltip?>"></i>
				<?php endif;?>

				<?php if($champ->required):?>
					<span class="rouge">*</span>
				<?php endif;?>
			</label>
			<?php if($champ->tooltip_content):?>
				<i class="fa fa-question-circle btn-open-popover" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="<?php echo $champ->tooltip_content?>"></i>
			<?php endif;?>
		<?php endif;?>
	</div>

	<?php if($champ->layout == 'line'):?>
		<div class="col-sm-9">
	<?php else:?>
		<div class="col-sm-12">
	<?php endif;?>

		<?php
		$affiche_data_html = null;

		if(count($champ->data_html)){
			foreach ($champ->data_html as $key => $data_html){

				$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
			}
		}
		?>

		<?php

			if(!$champ->default || h_value($champ->element,$champ->nom) != '') {
				$set_value = h_value($champ->element,$champ->nom);
			}else {
				$set_value = $champ->default;
			}

			if($champ->force_default) {
				$set_value = $champ->default;
			}

		?>

		<select id="<?php echo $champ->id?>" name="<?php echo $champ->nom?>" <?php if($affiche_data_html != null) echo $affiche_data_html; ?> class="form-control <?php echo $champ->class?>" <?php if($champ->multiple):?>multiple="multiple"<?php endif;?> <?php if($champ->disabled):?> dis <?php endif;?>>
			<?php
				if(!is_array($champ->options)) {
					switch($champ->options) {
						case 'ouinon':
							$champ->options = array(1=>'Oui', 0=>'Non');
						break;

						case 'nonoui':
							$champ->options = array(0=>'Non', 1=>'Oui');
						break;

						case 'civilite':
							$champ->options = array('Mme'=>'Mme', 'M.'=>'M.');
						break;

						case 'annees':
							$champ->options = array('2015'=>'2015', '2016'=>'2016', '2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020');
						break;

						case 'mois':
							$champ->options = array('01'=>'Janvier', '02'=>'Février', '03'=>'Mars', '04'=>'Avril', '05'=>'Mai', '06'=>'Juin', '07'=>'Juillet', '08'=>'Août', '09'=>'Septembre', '10'=>'Octobre', '11'=>'Novembre', '12'=>'Décembre');
						break;
					}
				}
			?>

			<?php foreach($champ->options as $key=>$val):?>
				<?php
					if(!$champ->multiple) {
						// Cas général
						if(!$champ->default || h_value($champ->element,$champ->nom) != '') {
							$default = h_value($champ->element,$champ->nom) == $key;
						}else {
							$default = $champ->default == $key;
						}
					}else {
						// Cas particulier pour les champs select multiple

						if($champ->options_selected){
							$default = in_array($key, $champ->options_selected);
						}else{
							$default = in_array($key, $champ->element->{str_replace('[]','',$champ->nom)});
						}
					}
				?>

				<?php if(is_array($val)):?>
					<optgroup label="<?php echo $key?>">

					<?php foreach ($val as $key2 => $val2): ?>

						<?php if($champ->multiple) {
							//On gère les valeur par defaut quand il y a les optgroup
							if($champ->options_selected){
								$default = in_array($key2, $champ->options_selected);
							}else{
								$default = in_array($key2, $champ->element->{str_replace('[]','',$champ->nom)});
							}
						} ?>


						<option value="<?php echo $key2?>" {{old($champ->nom,$set_value) == $key2 || $default ? 'selected' : '' }} ><?php echo $val2?></option>
					<?php endforeach;?>
					</optgroup>
				<?php else:?>
					<option value="<?php echo $key?>" {{old($champ->nom,$set_value) == $key || $default ? 'selected' : '' }} ><?php echo $val?></option>
				<?php endif;?>

			<?php endforeach;?>
		</select>

		<?php if($champ->element->{$champ->nom} && $champ->link):?>
			<p>
				<a href="{{ route('backoffice::'.$champ->link.'::create', [$champ->element->{$champ->nom}]) }}" target="_blank">
					&raquo; Accéder à la fiche
				</a>
			</p>
		<?php endif;?>
	</div>

	<?php if($champ->show_error):?>
		<div class="col-12">
			<p class="error-empty">

			</p>
		</div>
	<?php endif;?>
</div>
