<?php
$affiche_data_html = null;

if(count($champ->data_html)){
	foreach ($champ->data_html as $key => $data_html)	{
		$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
	}
}
?>
<div class="element form-group <?php if($errors->has($champ->nom) != ''):?>form-error<?php endif;?>" id="formelement-<?php echo $champ->nom?>">
	@if ($champ->show_label)
		<label for="<?php echo $champ->nom?>">
			<?php echo $champ->libelle?>

			<?php if($champ->required):?>
				<span class="rouge">*</span>
			<?php endif;?>
		</label>
	@endif

	<input type="file" class="type-text <?php echo $champ->class?>" id="<?php echo $champ->nom?>" name="<?php echo $champ->nom?>" <?php if($affiche_data_html != null) echo $affiche_data_html; ?>  />
</div>
