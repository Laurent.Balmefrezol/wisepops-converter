<div class="element form-group <?php if($errors->has('g-recaptcha-response') != ''):?>form-error<?php endif;?> " id="formelement-g-recaptcha-response">
    <div class="g-recaptcha"
        data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
    </div>

	<p class="error-empty">

	</p>
</div>
