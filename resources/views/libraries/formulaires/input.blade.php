<div class="element form-group <?php if($errors->has($champ->nom) != ''):?>form-error<?php endif;?> layout-<?php echo $champ->layout?> <?php if($champ->hidden): ?> dnone <?php endif; ?>" id="formelement-<?php echo $champ->nom?>">
	<?php if($champ->show_label):?>
		<label for="<?php echo $champ->nom?>">
			<?php echo $champ->libelle?>

			<?php if($champ->required):?>
				<span class="rouge">*</span>
			<?php endif;?>
		</label>
		<?php if($champ->tooltip_content):?>
			<i class="fa fa-question-circle btn-open-popover" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="<?php echo $champ->tooltip_content?>"></i>
		<?php endif;?>

		@if ($champ->details)
			<p class="fs-12">
				{{ $champ->details }}
			</p>
		@endif

	<?php endif;?>

	<?php

		if(!$champ->default || h_value($champ->element,$champ->nom) != '') {
			$set_value = h_value($champ->element,$champ->nom);
		}else {
			$set_value = $champ->default;
		}

		if($champ->force_default) {
			$set_value = $champ->default;
		}

		$affiche_data_html = null;

		if(count($champ->data_html)){
			foreach ($champ->data_html as $key => $data_html)	{
				$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
			}
		}

		if($set_value instanceof DateTime && $champ->datepicker){
			$set_value = $set_value->format($champ->date_format);
		}

		if($set_value instanceof DateTime && $champ->datetimepicker){
			$set_value = $set_value->format($champ->datetime_format);
		}
	?>

	<?php if($champ->show_input):?>
		<input  type="<?php if($champ->type=='input'):?>text<?php else:?>password<?php endif;?>"
				class="form-control
						type-text
						<?php if($champ->datepicker):?>datepicker<?php endif;?>
						<?php if($champ->datetimepicker):?>datetimepicker<?php endif;?>
						<?php echo $champ->class?>"
				id="<?php echo $champ->nom?>"
				name="<?php echo $champ->nom?>"
				<?php if($champ->maxlength): ?>
					maxlength="<?php echo $champ->maxlength?>"
				<?php endif;?>
				value="@if($champ->type == 'input'){{old($champ->nom, $set_value)}}@endif<?php if($champ->type == 'password' && $champ->default): ?> {{ $champ->default }}<?php endif; ?>"
				<?php if($champ->type == 'password'):?>autocomplete="off"<?php endif;?> <?php if($champ->placeholder):?>placeholder="<?php echo $champ->placeholder?>"<?php endif;?> <?php if($champ->disabled):?>disabled<?php endif;?> <?php if($affiche_data_html != null) echo $affiche_data_html; ?>  <?php if($champ->readonly):?>readonly<?php endif;?> />
	<?php else:?>
		@if($champ->type == 'input') {{old($champ->nom, $set_value)}} @endif
	<?php endif;?>

	<?php if($champ->show_error):?>
		<p class="error-empty">

		</p>
	<?php endif;?>
</div>
