<!-- Champs cachés pour la photo -->
@if(isset($image_uploaded))
	<input type="hidden" id="image_id" name="image_id" value="{{old('image_id', $image_uploaded->image->id)}}">
@elseif(isset($element->image))
	<input type="hidden" id="image_id" name="image_id" value="{{old('image_id', $element->image_id)}}">
@else
	<input type="hidden" id="image_id" name="image_id" value="{{old('image_id')}}">
@endif

<input type="hidden" id="x1" name="x1">
<input type="hidden" id="y1" name="y1">
<input type="hidden" id="height" name="height">
<input type="hidden" id="width" name="width">
<input type="hidden" id="dim_ratio_0" name="dim_ratio_0" value="{{$dim_ratio[0]}}">
<input type="hidden" id="dim_ratio_1" name="dim_ratio_1" value="{{$dim_ratio[1]}}">
<input type="hidden" id="width_display" name="width_display">
<!-- Champs cachés pour la photo -->

<div class="row">
	<div class="col-sm-12" id="inputimage">
		<label for="image" class="float-left">Choisir une nouvelle image :</label>

		<div class="float-left margeur-l10">
			<input type="hidden" id="fileupload_action" value="{{url('ajax/image/upload/'.$module)}}" />
			<input type="file" name="image" id="image" />
		</div>

		<div class="clearer"></div>
	</div>
</div>

@if(isset($image_uploaded))
	<div class="element-{{$element->id}}">
		<img id="jcrop" src="{{url('/').'/upload/images/'.$module.'/'.$image_uploaded->name}}" class="img-ajax" style="width: 100%">
	</div>
@elseif($element->image->exists)
	<div class="element-{{$element->id}} aright margeur-15">
		<div>
			<img id="jcrop" src="{{$element->img_url('original')}}" class="img-ajax" style="width: 100%">
		</div>

		<div class="margeur-10 btn-action-img">

			@if ($show_update)
				<a href="{{ route('backoffice::'.$module.'::updateThumb', [$element]) }}" class="btn btn-md btn-success float-left">
					<i class="fa fa-picture-o"></i> Recadrer la vignette
				</a>
			@endif

			@if ($show_delete)
				<a href="#" data-id-element="{{$element->id}}" data-popup-confirm-message="Supprimer l'image ?" data-popup-confirm-url="{{url('ajax/image/delete/'.$module.'/'.$element->id)}}" data-toggle="modal" data-target=".modal-popup-confirm" class="float-right popup-confirm btn btn-danger" title="Supprimer l'image">
					<i class="fa fa-trash-o"></i> Supprimer la photo
				</a>
			@endif

			<div class="clearer"></div>
		</div>
	</div>
@else
	<img class="img-ajax" src="" style="width: 100%; display:none;">
@endif
