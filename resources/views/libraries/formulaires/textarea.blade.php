<?php
$affiche_data_html = null;

if(count($champ->data_html)){
	foreach ($champ->data_html as $key => $data_html) {
		$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
	}
}
?>
<div class="row element form-group <?php if($errors->has($champ->nom) != ''):?>form-error<?php endif;?> layout-<?php echo $champ->layout?>" id="formelement-<?php echo $champ->nom?>">
	<?php if($champ->layout == 'line'):?>
		<div class="col-sm-5">
	<?php else:?>
		<div class="col-sm-12">
	<?php endif;?>

		<?php if($champ->show_label):?>
			<label for="<?php echo $champ->nom?>">
				<?php echo $champ->libelle?>

				<?php if($champ->required):?>
					<span class="rouge">*</span>
				<?php endif;?>
			</label>
			<?php if($champ->tooltip_content):?>
				<i class="fa fa-question-circle btn-open-popover" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="<?php echo $champ->tooltip_content?>"></i>
			<?php endif;?>
		<?php endif;?>
	</div>

	<?php if($champ->layout == 'line'):?>
		<div class="col-sm-7">
	<?php else:?>
		<div class="col-sm-12">
	<?php endif;?>

		<?php
			if(!$champ->default || h_value($champ->element,$champ->nom) != '') {
				$set_value = h_value($champ->element,$champ->nom);
			}else {
				$set_value = $champ->default;
			}
		?>
		<textarea id="<?php echo $champ->nom?>" <?php if($affiche_data_html != null) echo $affiche_data_html; ?>  class="form-control <?php echo $champ->class?>" <?php if($champ->maxlength):?>maxlength="<?php echo $champ->maxlength?>"<?php endif;?> name="<?php echo $champ->nom?>" style="<?php if($champ->width) echo 'width:'.$champ->width.';'?> <?php if($champ->height) echo 'height:'.$champ->height.';'?>" <?php if($champ->placeholder):?>placeholder="<?php echo $champ->placeholder?>"<?php endif;?> <?php if($champ->readonly):?> readonly <?php endif;?>><?php echo old($champ->nom,$set_value)?></textarea>
	</div>

	<?php if($champ->show_error):?>
		<div class="col-12">
			<p class="error-empty">

			</p>
		</div>
	<?php endif;?>
</div>
