<?php
	$affiche_data_html = null;
	if(count($champ->data_html)){
		foreach ($champ->data_html as $key => $data_html){
			$affiche_data_html .= ' data-'.$key.'="'.$data_html.'" ';
		}
	}
?>

<div class="element form-group <?php if($errors->has($champ->nom) != ''):?>form-error<?php endif;?>" id="formelement-<?php echo $champ->nom?>">
	<?php if($champ->show_label):?>
		<label for="<?php echo $champ->nom?>">
			<?php echo $champ->libelle?>

			<?php if($champ->required):?>
				<span class="rouge">*</span>
			<?php endif;?>

			<?php if($champ->show_error):?>
				<?php echo $errors->first($champ->nom)?>
			<?php endif;?>
		</label>
		<?php if($champ->tooltip_content):?>
			<i class="fa fa-question-circle btn-open-popover" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="<?php echo $champ->tooltip_content?>"></i>
		<?php endif;?>
	<?php endif;?>

	<textarea name="<?php echo $champ->nom?>" id="<?php echo $champ->nom?>" <?php if($affiche_data_html != null) echo $affiche_data_html; ?> ><?php echo htmlspecialchars(old($champ->nom,h_value($champ->element,$champ->nom)))?></textarea>

	@php
		$ckeditor = [
			'id' => $champ->nom,
			'path'	=>	'js/ckeditor',
			'config' => [
				'toolbar' => "",
				'height' => $champ->height
			]
		];
		
		echo h_display_ckeditor($ckeditor);
	@endphp

	<?php if($champ->show_error):?>
		<div class="">
			<p class="error-empty">

			</p>
		</div>
	<?php endif;?>

	@if ($champ->autosave)
		<p class="saving" style="display:none">
			<i class="fa fa-spinner fa-spin fa-fw"></i> {{ __('Sauvegarde...') }}
		</p>

		<p class="saved" style="display:none">
			<i class="fa fa-check fa-fw"></i> {{ __('Sauvegardé !') }}
		</p>
	@endif
</div>
