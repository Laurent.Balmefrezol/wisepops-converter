const mix = require('laravel-mix');

let public_theme_css = 'public/themes/insitio/css';
let public_theme_js = 'public/themes/insitio/js';

mix.sass('resources/scss/template.scss', public_theme_css);
mix.sass('resources/scss/forms.scss', public_theme_css);
mix.sass('resources/scss/styles.scss', public_theme_css);
mix.sass('resources/scss/login.scss', public_theme_css);

mix.js('resources/js/jquery.general.js', public_theme_js);
mix.js('resources/js/dashboard.js', public_theme_js);

if (mix.inProduction()) {
    mix.version();
}
