<?php
namespace Tests;

use App\Models\User;
use App\Models\Revenu;
use App\Models\Vendor;
use App\Models\Account;

trait Helpers
{

    protected function createUser($attributes = null) : User
    {

        if($attributes == null){
            $user = factory(User::class)->create();
        }else{
            $user = factory(User::class)->create($attributes);
        }

        return $user;
    }

    protected function signIn($attributes = null)
    {
        $this->actingAs($user = $this->createUser($attributes));

        return $this;
    }

}
