<?php

namespace Tests;

use Dotenv\Dotenv;

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, Helpers;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        //static::startChromeDriver(); //comment to fix Docker problem
    }

    public static function basePath($path = '')
    {
        return __DIR__ . '/../' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }


    public static function setUpBeforeClass()
     {
         copy(self::basePath('.env'), self::basePath('.env.backup'));
         copy(self::basePath('.env.dusk.local'), self::basePath('.env'));

         // (new Dotenv(self::basePath()))->overload();

         parent::setUpBeforeClass();
     }

     public static function tearDownAfterClass(): void
     {
         copy(self::basePath('.env.backup'), self::basePath('.env'));
         unlink(self::basePath('.env.backup'));

         // (new Dotenv(self::basePath()))->overload();

         parent::tearDownAfterClass();
     }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--no-sandbox', // Fix docker
            '--window-size=1920,1080',
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }


}
