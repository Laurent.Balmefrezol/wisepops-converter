<?php
namespace Tests\Unit;

use App\Libraries\Fixer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FixerTest extends TestCase
{
    /**
     * @var Faker\Generator
     *
     */
    protected $faker;

//    use RefreshDatabase;

    /** @test */
    public function fixer_io_connection_with_key(){

//        $this->withoutExceptionHandling();

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', config('fixer-io.full_url'));
        $this->assertEquals($res->getStatusCode(),200);

    }

    /** @test */
    public function create_fixer_object(){

        $good_cart = [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ];

        $fixer = new Fixer($good_cart['checkoutCurrency'],$good_cart['items']);

        $this->assertInstanceOf(Fixer::class,$fixer);
    }

}
