<?php


namespace Tests\Collection;


trait WrongCarts
{

    protected $wrong_carts = [
        //items is empty
        [
            'items' => [
                ],
            'checkoutCurrency' => 'EUR'
        ],
        //checkoutCurrency is required
        [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
        ],
        //currency is required
        [
            'items' => [
                '42' => [
                    'price' => 49.99,
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ],
        //price is required
        [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ],
        //quantity is required
        [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ],
        //quantity is integer
        [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                    'quantity' => 3.2
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ],
        //price is number
        [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 'dsds',
                    'quantity' => 3.2
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ],
    ];

}