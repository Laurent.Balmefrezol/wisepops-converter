<?php
namespace Tests\Feature;

use Tests\Collection\WrongCarts;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckoutTest extends TestCase
{
    use WrongCarts;

    /**
     * @var Faker\Generator
     *
     */
    protected $faker;

//    use RefreshDatabase;

    /** @test */
    public function good_cart_price(){

        $this->withoutExceptionHandling();

        $good_cart = [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                     'price' => 49.99,
                     'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR'
        ];


        $response = $this->json(
            'POST',
            route('checkout::price'),
            $good_cart
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'checkoutPrice' => true,
                'checkoutCurrency' => true,
            ]);
    }

    /** @test */
    public function wrong_carts(){

        $this->withoutExceptionHandling();

        foreach ($this->wrong_carts as $wrong_cart ){

            $response = $this->json(
                'POST',
                route('checkout::price'),
                $wrong_cart
            );

            $response->assertStatus(422);
        }
    }

}
