<?php
//Fixer.io config file
#FIXER IO
return [
    'key' => env('FIXER_KEY'),
    'url' => env('FIXER_URL'),
    'full_url' => env('FIXER_URL').env('FIXER_KEY'),
];